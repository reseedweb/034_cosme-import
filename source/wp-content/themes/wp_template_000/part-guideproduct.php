<div class="primary-row clearfix"><!-- begin primary-row -->
    <h2 class="mb20"><img src="<?php bloginfo('template_url'); ?>/img/content/top_box_title.jpg" alt="top" /></h2>
    <div class="message-group message-col240"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col top-box-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_img1.jpg" alt="top" />                
                <h3 class="box-title">化粧品輸入代行</h3> 
				<div class="box-text">
					化粧品輸入代行.comは化粧品輸入代行サービスを主業務としております。長年の経験によるノウハウと世界16カ国以上から様々なタイプの化粧品を輸入代行させていただいた実績がございますので安心してお任せください。
					<div class="box-btn">
						<a href="<?php bloginfo('url'); ?>/import">
							<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_btn.jpg" alt="top" />
						</a>
					</div>
				</div>				
            </div><!-- end message-col -->
             <div class="message-col top-box-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_img2.jpg" alt="top" />                
                <h3 class="box-title">品質管理</h3> 
				<div class="box-text">
					化粧品輸入代行.輸入化粧品の品質管理を目的とした規制成分定量分析を低価格・短納期でサービスを行っております。弊社独自の確かな経験により品質を維持しながらもローコスト・スピードサービスを提供しております。
					<div class="box-btn">
						<a href="<?php bloginfo('url'); ?>/analyze">
							<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_btn.jpg" alt="top" />
						</a>
					</div>
				</div>
            </div><!-- end message-col -->
             <div class="message-col top-box-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_img3.jpg" alt="top" />                
                <h3 class="box-title">薬事法チェック</h3> 
				<div class="box-text">
					化粧品の広告表現は薬事法で規制されていますが、適法範囲で最大限広告効果を発揮できる文面をご依頼主様と相談しながら作り上げております。
					<div class="box-btn">
						<a href="<?php bloginfo('url'); ?>/check">
							<img src="<?php bloginfo('template_url'); ?>/img/content/top_box_btn.jpg" alt="top" />
						</a>
					</div>
				</div>
            </div><!-- end message-col -->     
        </div><!-- end message-row -->
    </div><!-- end message-group -->
</div><!-- end primary-row -->