			<section id="maintop"><!-- begin maintop -->   
				<div class="maintop-group maintop-col340 wrapper"><!-- begin maintop-group -->
					<div class="maintop-row clearfix"><!--maintop-row -->
						<div class="maintop-col">
							<a href="<?php bloginfo('url'); ?>/import">
								<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img1.jpg" alt="maintop" />
							</a>
							<div class="maintop-title">
								<p class="maintop-small">Service 1</p>
								<p class="maintop-large">化粧品輸入代行</p>	
							</div>							
							<div class="maintop-text">
								ご依頼主様には純粋に<span class="maintop-clr">ビジネス</span>に<br />集中していただけるよう<span class="maintop-clr">サポート</span>!!
							</div><!-- end image -->												          
						</div><!-- end maintop-col -->	

						<div class="maintop-col">
							<a href="<?php bloginfo('url'); ?>/analyze">
								<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img2.jpg" alt="maintop" />
							</a>
							<div class="maintop-title">
								<p class="maintop-small">Service 2</p>
								<p class="maintop-large">適法管理</p>	
							</div>							
							<div class="maintop-text">
								独自のノウハウにより効率よく適法確認<br /><span class="maintop-clr">ローコスト・スピードサービス</span>を提供!!
							</div><!-- end image -->												          
						</div><!-- end maintop-col -->	
						
						<div class="maintop-col">
							<a href="<?php bloginfo('url'); ?>/check">
								<img src="<?php bloginfo('template_url'); ?>/img/top/top_box_img3.jpg" alt="maintop" />
							</a>
							<div class="maintop-title">
								<p class="maintop-small">Service 3</p>
								<p class="maintop-large">薬事法チェック</p>	
							</div>							
							<div class="maintop-text">
								薬事法の規制の範囲で最大限可能な<br />広告表現等への<span class="maintop-clr">ご提案・アドバイス</span>!!
							</div><!-- end image -->												          
						</div><!-- end maintop-col -->		
					</div><!-- end maintop-row -->
				</div><!-- end maintop-group -->
			</section><!-- end maintop -->