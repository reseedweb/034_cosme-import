<?php get_header(); ?>
<div class="primary-row clearfix">
	<h2 class="h2_title">製作事例</h2>
	<div id="message-work" class="message-group message-col370 clearfix">		
	<?php	
	$work_posts = get_posts( 	array(
	    'post_type'=> 'work',
	    'posts_per_page' => 4,
	    'paged' => get_query_var('paged'),
	));
	$i = 0;
	foreach($work_posts as $work_post):
	?>	
    <?php $i++; ?>
    <?php if($i%2 == 1) : ?>
	<div class="message-row clearfix">		
	<?php endif; ?>		
        <div class="message-col">        
			<h3 class="h3_title"><?php echo $work_post->post_title; ?></h3>
			<div class="image">
				<a href="<?php the_permalink(); ?>">
				<?php echo get_the_post_thumbnail($work_post->ID,'medium'); ?>
				</a>
			</div>
			<div class="work-ctf-table">
				<table>		
					<tr>
						<th>箱形状</th>
						<td><?php echo get_field('ctf_01'); ?></td>
					</tr>
					<tr>
						<th>厚み</th>
						<td><?php echo get_field('ctf_02'); ?></td>
					</tr>
					<tr>
						<th>表面色</th>
						<td><?php echo get_field('ctf_03'); ?></td>
					</tr>
					<tr>
						<th>印刷面</th>
						<td><?php echo get_field('ctf_04'); ?></td>
					</tr>
					<tr>
						<th>色数	</th>
						<td><?php echo get_field('ctf_05'); ?></td>
					</tr>																
				</table>
			</div>
			<div class="text">
				<?php echo get_field('description'); ?>
			</div>
		</div><!-- end col -->
    <?php if($i%2 == 0 || $i == count($posts) ) : ?>
    </div>
    <?php endif; ?>
	<?php endforeach; ?>
	</div>		
    <div class="primary-row text-center">
        <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
    </div>
    <?php wp_reset_query(); ?>	
</div>
<?php get_footer(); ?>