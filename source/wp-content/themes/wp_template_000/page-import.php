<?php get_header(); ?>	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">化粧品輸入代行</h3>		
		<p>外国から化粧品を輸入するには「化粧品製造販売業」と「化粧品製造業（包装・表示・保管）」の許可が必要です。</p>
		<p>その許可を取得するには薬剤師または同等の資格を持った常勤スタッフを雇用しなければならないなど、許可を取得・継続するには大きな固定費が必要となります。</p>
        <p class="mb10">また、許可を受けた業者として下記のような業務を、責任を持って行う必要があります。</p>
		<ul class="import-list">
			<li>1.許認可取得 / 維持管理</li>
			<li>2.商品毎の届出・法定書類の作成 / 記録 / 管理</li>
			<li>3.規制準拠チェック (薬事法準拠の成分内容であるかの確認)等</li>
			<li>4.許可業者名義での輸入通関</li>			
			<li>5.法定の表示作業	</li>			
		</ul>
		<p class="mt10">化粧品輸入代行.infoはこれらの複雑な行政対応業務・薬事法関連業務をリーズナブルな費用でご依頼主様に代わって行い、ご依頼主様には純粋にビジネスに集中していただけるようサポートさせていただいております。</p>
		<p>ご依頼主様が海外メーカーからの買い付けや国内での販路拡大等に最大限注力いただける環境を作り上げることが化粧品輸入代行.infoの第一の目標です。</p>
		<p class="import-note">化粧品輸入代行は、面倒なやり取りすべて引き受けます。</p>
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<p><img src="<?php bloginfo('template_url'); ?>/img/content/import_content_img1.jpg" alt="import" /></p>
		<p class="mt20"><img src="<?php bloginfo('template_url'); ?>/img/content/import_content_img2.jpg" alt="import" /></p>
		<p class="mt20"><img src="<?php bloginfo('template_url'); ?>/img/content/import_content_img3.jpg" alt="import" /></p>
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">化粧品輸入には「化粧品製造販売業許可」が必要</h3>
		<div class="message-right message-305 clearfix">
			<div class="image">
			    <img src="<?php bloginfo('template_url'); ?>/img/content/import_content_img4.jpg" alt="import" />
			</div>
			<div class="text">
				<p>「化粧品製造業許可」「化粧品製造販売業許可」は、数量の大小や、ハンドキャリーや国際宅配便・海上輸送と輸送方法に関係なく、海外から輸入する場合は必ず必要です。</p>
				<p>ネットオークションやフリーマーケットでの販売だけでなく、自らのサロン等で使用する場合も必要です。</p>
			</div>
		</div><!-- end message-305 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">輸入化粧品は薬事法に準拠</h3>
		<div class="message-right message-305 clearfix">
			<div class="image">
			    <img src="<?php bloginfo('template_url'); ?>/img/content/import_content_img5.jpg" alt="import" />
			</div>
			<div class="text">
				<p>化粧品を輸入するには、薬事法に準拠する必要があります。</p>
				<p>基本的に製造販売業などの「業許可（ライセンス）」を持つ業者しか輸入は認められていません。</p>
				<p>さらに、配合成分の チェックと行政への輸入販売等の届出、通関、全成分表示を経て販売が可能になります。</p>
				<p>これらの専門的で 煩雑な薬事法輸入販売の準拠業務一式を代行するのが、化粧品輸入代行の化粧品輸入代行.infoです。</p>
			</div>
		</div><!-- end message-305 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">化粧品の成分分析の流れ</h3>
		<div class="importcomponent-group importcomponent-col160 wrapper"><!-- begin importcomponent-group -->
			<div class="importcomponent-row importcomponent-bg clearfix"><!--importcomponent-row -->
				<div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">1</span></h4>
					<div class="importcomponent-text">
						製品情報収集/<br />infoへ<br />通知
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
		                <div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">2</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						輸入可否調査
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="import-other-col">					
					<div class="import-other-info">
						<h4 class="other-title"><i class="fa fa-circle-o"></i>輸入可報告</h4>
						<div class="other-text">要確認事項有の場合は<br />情報収集/アインズラボへ通知</div>
					</div>
					<div class="import-other-info mt10">
						<h4 class="other-title"><i class="fa fa-times"></i>輸入不可報告</h4>
						<div class="other-text">メーカーと原因確認や成分<br />変更・パッケージ変更等の交渉</div>
					</div>					
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">3</span></h4>
					<div class="importcomponent-text importcomponent-text-s16">
						基本業務委託契約締結<br />※基本内容に応じて業務<br />委託費用お支払
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="import-space"><img src="<?php bloginfo('template_url'); ?>/img/content/import_space.jpg" /></div>
			</div><!-- end importcomponent-row -->
			
			<div class="importcomponent-row importcomponent-bg clearfix"><!--importcomponent-row -->
				<div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">4</span></h4>
					<div class="importcomponent-text">
						発注/発注内容を<br />infoへ<br />通知
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
		                <div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">5</span></h4>
					<div class="importcomponent-text">
						法定表示・届出に<br />必要な必要情報の<br />収集/infoへの<br />通知
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">6</span></h4>
					<div class="importcomponent-text importcomponent-text-s18">
						法定表示<br />原稿の印刷前<br />確認
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">7</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						各種届出完了
					</div>					
				</div><!-- end importcomponent-col -->	
				<div class="import-space"><img src="<?php bloginfo('template_url'); ?>/img/content/import_space.jpg" /></div>
			</div><!-- end importcomponent-row -->
			
			<div class="importcomponent-row importcomponent-bg clearfix"><!--importcomponent-row -->
				<div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">8</span></h4>
					<div class="importcomponent-text">
						日本国内<br />作業の仕様/<br />作業指示確認
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
		                <div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">9</span></h4>
					<div class="importcomponent-text">
						分析に関する<br />必要情報収集/<br />infoへ<br />通知
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">10</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						ロット分析
					</div>
				</div><!-- end importcomponent-col -->
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>				
				<div class="import-other-col">					
					<div class="import-other-oinfo">
						<h4 class="other-title"><i class="fa fa-circle-o"></i><span class="other-space">分析OK通知</span></h4>						
					</div>
					<div class="import-other-info mt10">
						<h4 class="other-title"><i class="fa fa-times"></i>分析ＮＧ通知</h4>
						<div class="other-text">メーカーとクレーム・商品改善<br />・返金等の交渉<</div>
					</div>					
				</div><!-- end importcomponent-col -->					
				<div class="import-space"><img src="<?php bloginfo('template_url'); ?>/img/content/import_space.jpg" /></div>
			</div><!-- end importcomponent-row -->
			
			<div class="importcomponent-row importcomponent-bg clearfix"><!--importcomponent-row -->
				<div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">11</span></h4>
					<div class="importcomponent-text importcomponent-text-s13">
						メーカーへ製品出荷指示<br />※ご希望により商品代金の<br />外国送金<br />（ご依頼主様より入金後）
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
		                <div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">12</span></h4>
					<div class="importcomponent-text">
						通関に必要な<br />情報収集/<br />infoへ<br />通知
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">13</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						薬事通関
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">14</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						薬事倉庫入庫
					</div>					
				</div><!-- end importcomponent-col -->	
				<div class="import-space"><img src="<?php bloginfo('template_url'); ?>/img/content/import_space.jpg" /></div>
			</div><!-- end importcomponent-row -->
			
			<div class="importcomponent-row importcomponent-bg clearfix"><!--importcomponent-row -->
				<div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">15</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						受入検査
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
		        <div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">16</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						薬事製造作業
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col importcomponent-clr">
					<h4 class="importcomponent-title">STEP<span class="title_num">17</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						出荷判定
					</div>
				</div><!-- end importcomponent-col -->	
				<div class="importcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" /></div>
				<div class="importcomponent-col">
					<h4 class="importcomponent-title">STEP<span class="title_num">18</span></h4>
					<div class="importcomponent-text importcomponent-text-s20">
						納品
					</div>					
				</div><!-- end importcomponent-col -->					
			</div><!-- end importcomponent-row -->
			<div class="importcomponent-bottom-note">
				<div class="importcomponent-bottom-clr1"></div>
				<span class="importcomponent-bottom-text1">許可業者（弊社）業務</span>
				<div class="importcomponent-bottom-clr2"></div>
				<span class="importcomponent-bottom-text2">ご依頼主様作業</span>
			</div>
			</div><!-- end importcomponent-group -->
	</div><!-- end primary-row -->
			
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>