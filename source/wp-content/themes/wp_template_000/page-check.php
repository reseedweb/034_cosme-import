<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">化粧品の広告表現薬事チェックについて</h3>
		<div class="message-right message-305 clearfix">
			<div class="image">
			    <img src="<?php bloginfo('template_url'); ?>/img/content/check_content_img.jpg" alt="check" />
			</div>
			<div class="text">
				<p>化粧品には効能効果として55の表現が広告基準で定められています。 化粧品輸入代行infoはご依頼主様がお客様にお伝えしたい使用方法や効能効果の案を頂き、薬事法の規制の範囲で最大限可能な表現へのご提案・アドバイスを行っております。</p>
				<p>特に外国製化粧品のパンフレットや商品説明書をそのまま翻訳しただけでは違法表現になり化粧品輸入に影響が出る可能性がありますので、翻訳も含めたチェック業務も行っております。</p>
			</div>
		</div><!-- end message-305 -->
		<table class="check-table-content">
			<tr>
				<th>化粧品製造販売業者の名称、住所</th>
				<td>許可を受けたとおりに記載。</td>
			</tr>
			<tr>
				<th>化粧品の名称</th>
				<td>「化粧品製造販売届書」に記載して届け出た名前です。</td>
			</tr>
			<tr>
				<th>製造番号又は製造記号</th>
				<td></td>
			</tr>
			<tr>
				<th>成分の名称</th>
				<td><span class="check-text-clr">全成分を表示します。</span>（承認を受けて表示しないこととしたものを除く）</td>
			</tr>
			<tr>
				<th>使用の期限</th>
				<td>
					（１）アスコルビン酸、そのエステルもしくはそれらの塩類又は酵素を含有する化粧品<br />
					（２）3年以内に性状及び品質が変化するおそれがあるもの
				</td>
			</tr>
			<tr>
				<th>用法、容量、取り扱いの注意</th>
				<td>添付文書に掲載することも可能です。</td>
			</tr>
			<tr>
				<th>外国特例承認取得者等の氏名等</th>
				<td>薬事法第19条の2の規定による承認を受けた化粧品のみ</td>
			</tr>
		</table><!-- check-table-content -->	                                                                                                                                                    
		<p class="mt10">適切な表示がされていないまま化粧品を市場に出荷してしまうと、自主回収や行政による回収命令の対象となるので注意が必要です。それを避けるためにも適切な薬事法チェックを行い適切な化粧品の広告を表示を行う必要があります。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">薬事法について</h3>
		<p>薬事法の目的は、「医薬品」「医薬部外品」「化粧品」「医療機器」の4種類ついての品質、有効性及び安全性の確保のために必要な規制を行う事を目的としています。</p>
		<p class="mb10">医薬品等の製造・販売・流通に関する規定はもちろん、医薬品等の表示・広告、薬局の開設に関する内容等についても定める法律で、化粧品 輸入の過程で大きくかかわってくる法律です。</p>
		<h4 class="h4-title">薬事法における「化粧品」の定義</h4>
		<p class="check-note1">薬事法抜粋第二条第3項</p>
		<p>化粧品とは、人の身体を清潔にし、美化し、魅力を増し、容貌を変え、又は皮膚若しくは毛髪をすこやかに保つために、身体に塗擦、散布その他これらに類似する方法で使用されることが目的とされている物で、人体に対する作用が緩和なものをいう。</p>
		<p>ただし、これらの使用目的のほかに、第一項（医薬品の定義）第二号又は第三号に規定する用途に使用されることもあわせて目的とされている物及び医薬部外品を除く。</p>
		<p class="check-note2">主な該当商品として、石けん、歯磨き剤、シャンプー、リンス、スキンケア用品、メイクアップ用品が上げられます。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">化粧品の成分分析の流れ</h3>
		<div class="checkcomponent-group checkcomponent-col160 wrapper"><!-- begin checkcomponent-group -->
			<div class="checkcomponent-row clearfix"><!--checkcomponent-row -->
				<div class="checkcomponent-col checkcomponent-space1">
					<h4 class="checkcomponent-title">STEP<span class="title_num">1</span></h4>
					<div class="checkcomponent-text">
						チェックご希望の<br />原稿をご送付<br />ください
					</div>
				</div><!-- end checkcomponent-col -->	
				<div class="checkcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/content/check_arrow.jpg" alt="checkcomponent" /></div>
		                <div class="checkcomponent-col">
					<h4 class="checkcomponent-title">STEP<span class="title_num">2</span></h4>
					<div class="checkcomponent-text">
						お見積書兼<br />請求書をお送り<br />致します
					</div>
				</div><!-- end checkcomponent-col -->	
				<div class="checkcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/content/check_arrow.jpg" alt="checkcomponent" /></div>
				<div class="checkcomponent-col">
					<h4 class="checkcomponent-title">STEP<span class="title_num">3</span></h4>
					<div class="checkcomponent-text check-text-s20">
						ご入金確認後<br />業務開始
					</div>
				</div><!-- end checkcomponent-col -->					
			</div><!-- end checkcomponent-row -->
			
			<div class="checkcomponent-row clearfix"><!--checkcomponent-row -->
				<div class="checkcomponent-col checkcomponent-space2 checkcomponent-clr">
					<h4 class="checkcomponent-title">STEP<span class="title_num">4</span></h4>
					<div class="checkcomponent-text check-text-s20">
						レポートを<br />納品
					</div>
				</div><!-- end checkcomponent-col -->	
				<div class="checkcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/content/check_arrow.jpg" alt="checkcomponent" /></div>
		                <div class="checkcomponent-col checkcomponent-clr">
					<h4 class="checkcomponent-title">STEP<span class="title_num">5</span></h4>
					<div class="checkcomponent-text check-text-s16" >
						ご質問対応等<br />アフターサービスも<br />丁寧に行って<br />おります
					</div>
				</div><!-- end checkcomponent-col -->						
				<div class="checkcomponent-bottom-note"><div class="checkcomponent-bottom-clr"></div><span class="checkcomponent-bottom-text">弊社業務</span></div>
			</div><!-- end checkcomponent-row -->			
		</div><!-- end checkcomponent-group -->		
	</div><!-- end primary-row -->
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>