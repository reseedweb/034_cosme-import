<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h3 class="h3-title">会社概要</h3>
		<table class="company_table">
			<tr>
				<th>会社名</th>
				<td>
					株式会社アインズラボ<br />
					AINS-LAB INC.
				</td>
			</tr>
			<tr>
				<th>本社住所</th>
				<td>〒650-0023 神戸市中央区栄町通五丁目1-17</td>
			</tr>
			<tr>
				<th>第一倉庫住所</th>
				<td>〒650-0011 神戸市中央区下山手通7-1-30-101</td>
			</tr>
			<tr>
				<th>連絡先</th>
				<td>
					TEL：078-341-9550 FAX：050-3488-7641<br />
					E-mail：contact@ainslab.com
				</td>
			</tr>
			<tr>
				<th>設　立</th>
				<td>40497</td>
			</tr>
			<tr>
				<th>従業員数</th>
				<td>27名</td>
			</tr>
			<tr>
				<th>取引銀行</th>
				<td>
					三井住友銀行<br />
					三菱東京UFJ銀行<br />
					尼崎信用金庫<br />
					播州信用金庫
				</td>
			</tr>
			<tr>
				<th>許可／倉庫</th>
				<td>
					神戸本社（神戸市中央区）化粧品製造販売業許可<br />

					第一倉庫（神戸市中央区）化粧品製造業（包装・表示・保管）許可<br />

					第二倉庫（神戸市東灘区）化粧品製造業（包装・表示・保管）許可<br />

					第三倉庫（神戸市須磨区）化粧品製造業（包装・表示・保管）許可<br />

					第四倉庫（兵庫県西宮市）化粧品製造業（包装・表示・保管）許可<br />

					第五倉庫（兵庫県姫路市）化粧品製造業（包装・表示・保管）許可<br />
					 
					第六倉庫（神戸市須磨区）化粧品製造業（包装・表示・保管）許可<br />

					提携倉庫　化粧品製造業（包装・表示・保管）許可<br />
					東京・千葉・神奈川・静岡・大阪の許可倉庫と提携しております。
			</td>
			</tr>
			<tr>
				<th>所属団体</th>
				<td>
					化粧品工業連合会　正会員<br />
					日本輸入化粧品協会　準会員
				</td>
			</tr>
						
		</table>
	</div>
	<div class="primary-row clearfix">
		<h3 class="h3-title">アクセス</h3>
		<div id="company_map">
			<iframe width="760" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps?f=q&amp;source=s_q&amp;hl=vi&amp;geocode=&amp;q=%E7%A5%9E%E6%88%B8%E5%B8%82%E4%B8%AD%E5%A4%AE%E5%8C%BA%E6%A0%84%E7%94%BA%E9%80%9A%E4%BA%94%E4%B8%81%E7%9B%AE1-17&amp;aq=&amp;sll=37.0625,-95.677068&amp;sspn=44.928295,86.572266&amp;ie=UTF8&amp;hq=&amp;hnear=Nh%E1%BA%ADt+B%E1%BA%A3n,+%E3%80%92650-0023+Hy%C5%8Dgo-ken,+K%C5%8Dbe-shi,+Ch%C5%AB%C5%8D-ku,+Sakaemachid%C5%8Dri,+5+Chome%E2%88%921%E2%88%9217+%E6%A0%84%E7%94%BA%E3%83%80%E3%82%A4%E3%83%A4%E3%83%8F%E3%82%A4%E3%83%84&amp;t=m&amp;z=14&amp;ll=34.684923,135.182902&amp;output=embed"></iframe>
		</div>
	</div>
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>