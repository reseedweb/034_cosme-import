	<div class="primary-row clearfix">
		<h3 class="h3-title">化粧品の成分分析の流れ?</h3>
		<div class="flowcomponent-group flowcomponent-col160 wrapper"><!-- begin flowcomponent-group -->
			<div class="flowcomponent-row clearfix"><!--flowcomponent-row -->
				<div class="flowcomponent-col">
					<h4 class="flowcomponent-title">STEP<span class="title_num">1</span></h4>
					<div class="flowcomponent-text">
						ご依頼主様<br />基本情報登録<br />（初回のみ）
					</div>
				</div><!-- end flowcomponent-col -->	
				<div class="flowcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" alt="flowcomponent" /></div>
		                <div class="flowcomponent-col">
					<h4 class="flowcomponent-title">STEP<span class="title_num">2</span></h4>
					<div class="flowcomponent-text">
						弊社規定様式で<br />検体リストを作成<br />弊社へご送付
					</div>
				</div><!-- end flowcomponent-col -->	
				<div class="flowcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" alt="flowcomponent" /></div>
				<div class="flowcomponent-col">
					<h4 class="flowcomponent-title">STEP<span class="title_num">3</span></h4>
					<div class="flowcomponent-text flow-text-s20">
						検体を弊社へ<br />ご送付
					</div>
				</div><!-- end flowcomponent-col -->	
				<div class="flowcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" alt="flowcomponent"/></div>
				<div class="flowcomponent-col">
					<h4 class="flowcomponent-title">STEP<span class="title_num">4</span></h4>
					<div class="flowcomponent-text flow-text-s14">
						弊社がお送りする<br />受託内容確認、お見積<br />もり金額提示メール<br />にご承認
					</div>
				</div><!-- end flowcomponent-col -->	
			</div><!-- end flowcomponent-row -->
			
			<div class="flowcomponent-row clearfix"><!--flowcomponent-row -->
				<div class="flowcomponent-col flowcomponent-clr">
					<h4 class="flowcomponent-title">STEP<span class="title_num">5</span></h4>
					<div class="flowcomponent-text flow-text-s20">
						分析開始
					</div>
				</div><!-- end flowcomponent-col -->	
				<div class="flowcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" alt="flowcomponent"/></div>
		                <div class="flowcomponent-col flowcomponent-clr">
					<h4 class="flowcomponent-title">STEP<span class="title_num">6</span></h4>
					<div class="flowcomponent-text flow-text-s20">
						予定納期に<br />速報を報告
					</div>
				</div><!-- end flowcomponent-col -->	
				<div class="flowcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" alt="flowcomponent"/></div>
				<div class="flowcomponent-col flowcomponent-clr">
					<h4 class="flowcomponent-title">STEP<span class="title_num">7</span></h4>
					<div class="flowcomponent-text flow-text-s14">
						随時または毎月<br />月末〆にて分析試験<br />成績書を発行<br />（請求書同封）
					</div>
				</div><!-- end flowcomponent-col -->	
				<div class="flowcomponent-arrow"><img src="<?php bloginfo('template_url'); ?>/img/common/flowcomponent_arrow.jpg" alt="flowcomponent"/></div>
				<div class="flowcomponent-col">
					<h4 class="flowcomponent-title">STEP<span class="title_num">8</span></h4>
					<div class="flowcomponent-text flow-text-s20">
						分析費用の<br />お支払い
					</div>					
				</div><!-- end flowcomponent-col -->	
			</div><!-- end flowcomponent-row -->
			<div class="flow-bottom-note"><div class="flow-bottom-clr"></div><span class="flow-bottom-text">弊社業務</span></div>
		</div><!-- end flowcomponent-group -->
		
	</div><!-- end primary-row -->	