<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h3 class="h3-title">よくある質問</h3>
		<div class="primary-row clearfix">
			<h4 class="h4-title">化粧品の輸入代行とは？</h4>
			<div class="faq-text">
				<p>化粧品を輸入する際に、厚生労働大臣の許可が必要となります。</p>
				<p>そこで、許可を持っていない会社に代わり、輸入元として薬事法に基づく品質管理や通関の手続きをする事などを輸入代行が担います。</p>
				<p>輸入代行を利用した場合は、自社で許可を取る必要がありませんので、薬剤師等の人件費がかからず、煩雑な薬事申請も必要ありません。</p>
			</div><!-- faq-text -->
		</div><!-- end primary-row -->		
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">初に必要な物は何でしょうか？</h4>
		<div class="faq-text">
			<p>輸入したい商品の全成分表、実物サンプル、商品の規格書などです。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">輸入するまでにはどれぐらい時間がかかりますか？</h4>
		<div class="faq-text">
			<p>輸送日数につきましては出荷国や船、飛行機など輸送方法によっても変わってきます。</p>
			<p>化粧品輸入代行をご利用の場合、急げば1ヶ月程で輸入が可能です</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">輸入代行を利用した場合、製品の表示はどのような記載になりますか？</h4>
		<div class="faq-text">
			<p>品の表示は製造販売元である輸入代行会社と御社との連名表示となります。</p>
			<p>[例]<br />
			製造販売元　　輸入元社名住所 <br />
			発　売　元　　御社名　住所<br />
			製品に御社名のみ記載して販売したい場合は、御社にて製造販売業の許可を取得する必要がございます。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">化粧品と医薬部外品の違いは何ですか？</h4>
		<div class="faq-text">
			<p>薬事法によって「医薬品」「医薬部外品」「化粧品」は分類され、それぞれ原料から製造方法、ラベルへの表示内容、広告の表現まで厳しく規制されています。</p>
			<p>医薬部外品というのは、化粧品と医薬品の中間的な製品で、ある特定の効能や効果について厚生労働省に承認された製品の事を言います。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">初めての化粧品輸入なのですが、初心者でも大丈夫でしょうか？</h4>
		<div class="faq-text">
			<p>はい、お任せ下さい。弊社は世界16カ国以上の国と化粧品輸入代行をさせていただいたノウハウがあり、どの他社よりも海外化粧品に精通していると自負しております。</p>
			<p>不安な点やご不明なところがあればお気軽にご相談ください。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">日本に輸送する際に商品が破損したら？</h4>
		<div class="faq-text">
			<p>輸送や船・飛行機・トラックと積み替え時に商品に衝撃が加わります。</p>
			<p>緩衝材をしっかり入れ荷崩れしにくいなど梱包をシッパーにきちんと確認してください。</p>
			<p>損害保険をかけてから出荷するようにしてください。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
		
	<div class="primary-row clearfix">
		<h4 class="h4-title">化粧品成分分析はなぜ行うのでしょうか?</h4>
		<div class="faq-text">
			<p>成分表通りの配合か、成分表に記載の無い原料や違反原料は入っていないかなどを調べます。</p>
			<p>成分表通りでないと薬事法違反となってしまいます。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">自社の社名をラベルに記載することはできますか？?</h4>
		<div class="faq-text">
			<p>はい、可能です。</p>
			<p>製造販売業者の社名・連絡先等と共にお客様を【発売元】として表記できます。 </p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">薬事法違反商品が発覚した場合？?</h4>
		<div class="faq-text">
			<p>表示ラベルに記載のない成分や規制のある成分が入ってる商品が発覚した場合は、速やかに自主回収または行政からの強制回収の指示があります。</p>
			<p>回収となると、回収費用がかかるばかりか販売不可にもなるので薬事法チェックは、怠るべきではありません。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<h4 class="h4-title">容器や化粧箱の表示内容はどういった物でしょうか？?</h4>
		<div class="faq-text">
			<p>化粧品の入った容器や中の容器が見えない化粧箱など外装の表示内容について、薬事法によって詳しく規定されその
			<p>表示は義務付けられています。</p>
			<p>薬事法で定められている化粧品の表示には、</p>
			<p class="pt20">
			 <span class="faq-space">①製造販売元の名称及び所在地<br /></span>
			　②販売名称<br />
			　③製造番号又は製造記号<br />
			　④全成分の表示<br />
			　⑤重量・容量又は個数のいずれか<br />
			　⑥使用期限（３年を超えて品質が安定な化粧品の場合は対象外）<br />
			　⑦種類別名称（販売名だけでは不明確な場合）<br />
			　⑧使用上及び取り扱い上の注意<br />
			　⑨容器の識別表示<br />
			などがあります。</p>
			<p>医薬部外品の場合は、基本的には、化粧品の表示と同じですが、成分表示方法と医薬部外品であることを明確にするところが異なります。</p>
		</div><!-- faq-text -->
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','flowcomponent'); ?>
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>