<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h3 class="h3-title">化粧品輸入の成分分析について</h3>
		<div class="message-right message-305 clearfix">
			<div class="image">
			    <img src="<?php bloginfo('template_url'); ?>/img/content/analyze_content_img1.jpg" alt="analyze" />
			</div>
			<div class="text">
				<p>化粧品は「化粧品基準」のネガティブリストやポジティブリストなどにおいて配合禁止成分や配合制限成分が薬事的に定められています。これらの成分規制は外国製・日本製に関わらず、薬事法で定義されている化粧品に該当する製品全てに適用されます。特に外国製化粧品については各国で規制の内容に違いがある為日本の禁止成分・制限成分が含まれている場合があります。化粧品輸入代行infoでは、パートナー分析所で、このような外国製化粧品に対する品質管理の為の規制成分定量分析を主とした様々な分析を行うことが可能です。</p>
				<p>弊社が薬事法に照らしてご希望製品の成分を拝見し、適切な成分分析をご提案させていただきます。</p>
			</div>
		</div><!-- end message-305 -->
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix">
		<div class="primary-row clearfix">
			<h3 class="h3-title">化粧品基準について</h3>
			<p>化粧品への配合の禁止や制限などの成分基準のこと。</p>
			<p>薬事法に基づく主に配合成分に対する具体的な規制のことで、内容は、以下の厚生労働省の告示に記載されています。</p>
			<p class="analyze-note">>>化粧品基準を見る</p>
			<p class="pt10"><img src="<?php bloginfo('template_url'); ?>/img/content/analyze_content_img2.jpg" alt="analyze" /></p>
			<p class="pt10">基準では、原料について「防腐剤、紫外線吸収剤、タール色素以外の成分の配合の禁止・制限」をしたネガティブリスト、及び「防腐剤、紫外線吸収剤、タール色素の配合を制限」したポジティブリストがあります。</p>
		</div><!-- end primary-row -->	
		
		<div class="primary-row clearfix">
			<h4 class="h4-title">ネガティブリスト</h4>
			<ul class="analyze-list">
				<li class="analyze-step">
					1.どの化粧品にも使ってはならない成分<br /><span class="analyze-step-small">（例えば　ホルマリン、クロロホルム等）</span>
				</li>
				<li class="analyze-step">
					2.すべての化粧品について使用量の制限がある成分<br /><span class="analyze-step-small">（トウガラシチンキ等）</span>
				</li>
				<li class="analyze-step">
					3.化粧品の種類又は使用目的により使用量の制限がある成分<br /><span class="analyze-step-small">（パラフェノールスルホンさん亜鉛等）</span>
				</li>
				<li class="analyze-step">
					4.化粧品の種類により使用量の制限がある成分<br /><span class="analyze-step-small">（チオクト酸、ユビデカレノン）</span>
				</li>
				<li class="analyze-step">
					5.医薬品成分は原則として配合禁止
				</li>
			</ul>			
			<p class="pt20">ポジティブリストでは、防腐剤、紫外線吸収剤、及びタール色素について下記のように規定してあります。</p>
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">
			<h4 class="h4-title">ポジティブリスト</h4>
			<ul class="analyze-list">
				<li class="analyze-step">
					1.すべての化粧品について使用量の制限がある成分<br /><span class="analyze-step-small">（パラベン類、サリチル酸ホモメンチル等）</span>
				</li>
				<li class="analyze-step">
					2.化粧品の種類により使用量の制限がある成分<br /><span class="analyze-step-small">（クロルフェネシン、パラメトキシケイ皮酸２－エチルヘキシル等）</span>
				</li>				
			</ul>			
			<p class="pt20">化粧品の輸入時は、ポジティブリスト・ネガティブリストで、成分が問題ないことを確認する必要があります。</p>
			<p>よって、化粧品の成分分析が可能な設備を所有する化粧品輸入代行infoのような会社に任せる必要があります。</p>
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	
	
	<div class="primary-row clearfix">
		<h3 class="h3-title">成分分析の種類</h3>
		<div class="primary-row clearfix">		
			<h4 class="h4-title">レギュレーション分析</h4>
			<div class="analyze-text">
				<p>薬事法に基づく「化粧品基準」に対する適合確認のための一斉分析です。</p>
				<p>化粧品への配合禁止成分や、ネガティブ・ポジティブリスト成分を対象とします。</p>
			</div><!-- faq-text -->
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">		
			<h4 class="h4-title">成分指定分析</h4>
			<div class="analyze-text">
				<p>製品に含まれる成分を1成分単位で分析します。</p>
			</div><!-- faq-text -->
		</div><!-- end primary-row -->
		
		<div class="primary-row clearfix">		
			<h4 class="h4-title">その他・試験分析</h4>
			<div class="analyze-text">
				<p>特定微生物試験・ヒトパッチテスト・長期保存試験・医薬部外品原料規格 など</p>
			</div><!-- faq-text -->
		</div><!-- end primary-row -->
	</div><!-- end primary-row -->
	<?php get_template_part('part','flowcomponent'); ?>	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>