<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h3 class="h3-title">ご依頼の流れ</h3>
		<div class="primary-row clearfix">
			<div class="flow-content-first">
				<div class="flow-step">step1</div>
				<div class="flow-info-first">
					<h4 class="flow-title pl20">お問い合わせ・ヒアリング</h4>
					<div class="flow-text pl20">
						<p>まずは、お電話・メールにて弊社までご連絡く
						ださい。</p>
						<p>ちょっとしたご質問、ご相談でも構いません。専門のスタッフが対応させていただきます。</p>
						<p>輸入した い商品、数量等について、お伺いします。</p>
					</div>
					<div class="flow-contact">
						<div class="flow-contact-btn">
							<a href="<?php bloginfo('url'); ?>/contact">
								<img src="<?php bloginfo('template_url'); ?>/img/content/flow_contact_btn.png" alt="flow" />
							</a>
						</div>						
					</div>
				</div><!-- ./flow-info-first -->
			</div><!-- ./flow-content-first -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow_first.png" alt="flow" />
			</div>
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step2">step2</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">化粧品 成分分析</h4>
							<div class="flow-text">
								<p>ご要望の製品が商品のサンプル及び成分表（配合成分とその成分配合量など、正確にわかるもの）をもとに日本で販売できる製品かどうか成分の確認を行い報告いたします。</p>
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow.png" alt="flow" />
			</div><!-- ./flow-arrow -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step3">step3</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">輸入税関手続き</h4>
							<div class="flow-text">
								<p>海外現地仕入れ先からの現地国内物流を手配、輸入通関手続きも弊社で代行致します。</p>
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow.png" alt="flow" />
			</div><!-- ./flow-arrow -->
		</div><!-- ./primary-row -->
	
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step3">step4</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">成分表示ラベルの作成・加工</h4>
							<div class="flow-text">
								<p>販売名称を決定し薬事法に基づくラベル表示内容の確認。</p>
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow.png" alt="flow" />
			</div><!-- ./flow-arrow -->
		</div><!-- ./primary-row -->
	
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step3">step5</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">契約</h4>
							<div class="flow-text">
								<p>お見積りに納得いただけましたら、本契約をお願いいたします。</p>
								<p>お客様のご都合に合わせて、作業日を決め着工に入っていきます。</p>
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow.png" alt="flow" />
			</div><!-- ./flow-arrow -->
		</div><!-- ./primary-row -->
	
	
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step3">step6</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">ロット分析</h4>
							<div class="flow-text">
								<p>輸入ロットを事前にメーカーよりいただき、輸入可能と判断された製品と同様の結果がでるか分析します。</p>								
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow.png" alt="flow" />
			</div><!-- ./flow-arrow -->
		</div><!-- ./primary-row -->
	
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step3">step7</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img6.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">輸入スタート</h4>
							<div class="flow-text">
								<p>輸入ロットを事前にメーカーよりいただき、輸入可能と判断された製品と同様の結果がでるか分析します。</p>								
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow.png" alt="flow" />
			</div><!-- ./flow-arrow -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step3">step8</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img7.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">出荷判定</h4>
							<div class="flow-text">
								<p>製品について、薬事法に基づくラベルを貼り、品質の確認作業を行います。</p>								
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_arrow.png" alt="flow" />
			</div><!-- ./flow-arrow -->
		</div><!-- ./primary-row -->
		
		<div class="primary-row clearfix">
			<div class="flow-content">
				<div class="flow-step flow-step3">step9</div>
				<div class="flow-info">
					<div class="message-right message-185 clearfix">
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img8.jpg" alt="flow" />
						</div>
						<div class="text">
							<h4 class="flow-title">納品</h4>
							<div class="flow-text">
								<p>お客様の元へ配送いたします。</p>								
							</div>	
						</div>
					</div><!-- end message-185 -->									
				</div><!-- ./flow-info -->
			</div><!-- ./flow-content -->			
		</div><!-- ./primary-row -->		
	</div><!-- end primary-row -->
	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>