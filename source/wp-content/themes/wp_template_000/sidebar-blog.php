<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-con"><!-- begin side-con -->
        <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_contact_bg.jpg" />
		<h2 class="side-con-title">INQUIRY</h2>
		<div class="side-con-text1">
			<p>ご質問、ご相談など<br />お気軽にお問い合わせください。</p>			
		</div>		
		<p class="side-con-text2">お急ぎの方は電話でも対応</p>
		<p class="side-con-text3">FAXからの依頼はこちらから</p>
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_contact_btn.png" />
            </a>
        </div><!-- ./side-con-btn -->		
    </div><!-- end side-con -->
	<div class="side-con-text">
	 <p>株式会社 アインズラボ AINS-LABINC.<br />神戸市中央区栄町通五丁目1-17</p>
	</div>
</div><!-- end sidebar-row -->

<?php get_template_part('part','snavi'); ?>

<div class="sidebar-row clearfix">
	<div class="sideBlog">
		<h4 class="sideBlog-title">最新の投稿</h4>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h4 class="sideBlog-title">最新の投稿</h4>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1&title_li='); ?>
	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h4 class="sideBlog-title">最新の投稿</h4>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div>
