<div class="sidebar-row clearfix">
	<aside id="sideNavi"><!-- begin sideNavi -->
		<h2 class="title">MENU</h2>
		<ul>
			<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
			<li><a href="<?php bloginfo('url'); ?>/beginner">初めての方へ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/analyze">成分分析</a></li>
			<li><a href="<?php bloginfo('url'); ?>/check">薬事法チェック</a></li>
			<li><a href="<?php bloginfo('url'); ?>/price">初回プライスキャンペーン</a></li>
			<li><a href="<?php bloginfo('url'); ?>/flow">ご依頼の流れ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/contact">問い合わせ</a></li>
			<li><a href="<?php bloginfo('url'); ?>/company">運営会社</a></li>
			<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>
			<li><a href="<?php bloginfo('url'); ?>/blog">ブログ</a></li>
		</ul>
	</aside><!-- end sideNavi -->
</div><!-- end sidebar-row -->