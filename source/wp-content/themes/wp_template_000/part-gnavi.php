			<nav>
                <div id="gNavi"  class="wrapper">
                    <ul class="dynamic clearfix">
                        <li>
							<a href="<?php bloginfo('url'); ?>/beginner">
								<span class="first">初めての方へ</span>
								<span class="second">BEGINNER</span>
							</a>
						</li>
						<li class="naviw2">
							<a href="<?php bloginfo('url'); ?>/import">
								<span class="first">化粧品輸入代行</span>
								<span class="second">COSMETICS IMPORT AGENCY</span>
							</a>
						</li>                            
						<li class="naviw1">
							<a href="<?php bloginfo('url'); ?>/analyze">
								<span class="first">成分分析</span>
								<span class="second">ANALYSIS</span>
							</a>
						</li>
						<li class="naviw2">
							<a href="<?php bloginfo('url'); ?>/check">
								<span class="first">薬事法チェック</span>
								<span class="second">CHECK</span>
							</a>
						</li>    
						<li class="naviw3">
							<a href="<?php bloginfo('url'); ?>/price">
								<span class="first">初回プライスキャンペーン</span>
								<span class="second">THE FIRST PRICE CAMPAIGN</span>
							</a>
						</li> 
						<li>
							<a href="<?php bloginfo('url'); ?>/flow">
								<span class="first">ご依頼の流れ</span>
								<span class="second">FLOW</span>
							</a>
						</li>
						<li class="navilast">
							<a href="<?php bloginfo('url'); ?>/contact">
								<span class="first">お問い合わせ</span>
								<span class="second">INQUIRY</span>
							</a>
						</li>					
                    </ul>
					<div class="navi-space"></div>
                </div>
            </nav>