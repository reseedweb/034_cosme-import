<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="mb20 mt10"><img src="<?php bloginfo('template_url'); ?>/img/content/top_content_flow.jpg" alt="top" /></h2>
	<div class="message-group message-col240"><!-- begin message-group -->
        <div class="message-row clearfix"><!--message-row -->
            <div class="message-col top-flow-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_step1.jpg" alt="top" />                
                <h3 class="flow-title">輸入可否調査</h3> 
				<div class="flow-text">
					まずは、ご要望の製品が日本で販売できる製品かどうか成分表の確認を行い報告いたします。
				</div>				
            </div><!-- end message-col -->
			<div class="message-col top-flow-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_step2.jpg" alt="top" />                
                <h3 class="flow-title">契約</h3> 
				<div class="flow-text">
					お客様と弊社の間で契約を締結させていただきます。
				</div>				
            </div><!-- end message-col -->
            <div class="message-col top-flow-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_step3.jpg" alt="top" />                
                <h3 class="flow-title">原稿の印刷前確認</h3> 
				<div class="flow-text">
					販売名称を決定し薬事法に基づくラベル表示内容の確認を行います。
				</div>				
            </div><!-- end message-col -->     
        </div><!-- end message-row -->
		
		<div class="message-row clearfix"><!--message-row -->
            <div class="message-col top-flow-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_step4.jpg" alt="top" />                
                <h3 class="flow-title">ロット分析</h3> 
				<div class="flow-text">
					輸入ロットのサンプルを事前にメーカーよりいただき、輸入可能と判断された成分表と相違がないか分析します。
				</div>				
            </div><!-- end message-col -->
			<div class="message-col top-flow-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_step5.jpg" alt="top" />                
                <h3 class="flow-title">輸入スタート</h3> 
				<div class="flow-text">
					薬事輸入通関の手続きを代行します。弊社の倉庫に製品が到着します。
				</div>				
            </div><!-- end message-col -->
            <div class="message-col top-flow-content">     
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_step6.jpg" alt="top" />                
                <h3 class="flow-title">出荷判定</h3> 
				<div class="flow-text">
					製品について、薬事法に基づくラベルを貼り、品質の確認作業を行います。
				</div>				
            </div><!-- end message-col -->     
        </div><!-- end message-row -->		
    </div><!-- end message-group -->
	<p class="pt20 text-center"><img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_arrow.jpg" alt="top" /></p>
	<div class="top-flow-step7">
		<img src="<?php bloginfo('template_url'); ?>/img/content/top_flow_step7.jpg" alt="top" />  
		<h3 class="flow-step7-title">納品</h3> 
		<div class="flow-step7-text">
			出荷判定が終わった後、お客様の元へ製品を納品致します。
		</div>		
	</div>
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<div class="top-content-contact"><!-- begin top-content-info5 -->
			<div class="top-contact-btn">
				<a href="<?php bloginfo('url'); ?>/contact">
					<img src="<?php bloginfo('template_url'); ?>/img/content/top_contact_btn.png" alt="top" />
				</a>
			</div>
		</div<!-- ./top-content-info5 -->
	</div><!-- end primary-row -->
</div><!-- end primary-row -->


