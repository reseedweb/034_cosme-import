<!DOCTYPE html>
<html>
    <head>
        <!-- meta -->        
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />         
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- title -->
        <title><?php wp_title( '|', true); ?></title>
        <meta name="robots" content="noindex,follow,noodp" />
        
        <link rel="profile" href="http://gmpg.org/xfn/11" />                
        
        <!-- global javascript variable -->
        <script type="text/javascript">
            var CONTAINER_WIDTH = '1090px';
            var CONTENT_WIDTH = '1060px';
            var BASE_URL = '<?php bloginfo('url'); ?>';
            var TEMPLATE_URI = '<?php bloginfo('template_url') ?>';
            var CURRENT_MODULE_URI = '';
            Date.now = Date.now || function() { return +new Date; };            
        </script>        
        <!-- Bootstrap -->
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php bloginfo('template_url'); ?>/css/bootstrap-theme.min.css" rel="stylesheet" />   
        <!-- fontawesome -->
        <link href="<?php bloginfo('template_url'); ?>/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_url'); ?>/js/html5shiv.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/respond.min.js"></script>
        <![endif]-->        
        
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.js" type="text/javascript"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/jquery.plugins.js" type="text/javascript"></script>               
        <script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type="text/javascript"></script>        	
		   
        <link href="<?php bloginfo('template_url'); ?>/style.css?<?php echo md5(date('l jS \of F Y h:i:s A')); ?>" rel="stylesheet" />
        <script src="<?php bloginfo('template_url'); ?>/js/config.js" type="text/javascript"></script>        
        <?php wp_head(); ?>
    </head>
    <body>     
        <div id="screen_type"></div>
        
        <div id="wrapper"><!-- begin wrapper -->

            <section id="top"><!-- begin top -->                
                <div class="wrapper top_content clearfix">
                    <h1 class="top-text">化粧品輸入代行・化粧品成分分析・薬事法チェックならお任せ下さい。</h1>
                    <ul class="top-links">
                        <li><a href="<?php bloginfo('url'); ?>/faq">よくあるご質問</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/company">運営会社</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a></li>                        
                    </ul>
                </div>                
            </section><!-- end top -->            

            <header><!-- begin header -->
               <div class="header_content  wrapper clearfix">      
					<div class="logo">
						<a href="<?php bloginfo('url'); ?>/">
							<img src="<?php bloginfo('template_url'); ?>/img/common/logo.jpg" alt="top" />
						</a>
					</div>
				</div>
			</header><!-- end header -->
				
				<?php get_template_part('part','gnavi'); ?>

				<?php if(is_home()) : ?>
					<?php get_template_part('part','slider'); ?>
				<?php get_template_part('part','maintop'); ?>
				<?php else : ?>
					<section id="page-feature">
						<div class="feature-info wrapper">
							<div class="page-feature-content wrapper clearfix">                        
								<?php if(is_page('faq')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">faq</span>
								</h2>
								<?php elseif(is_page('contact')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">contact</span>
								</h2>
								<?php elseif(is_page('company')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">company</span>
								</h2>
								<?php elseif(is_page('privacy')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">pravicy</span>
								</h2>
								<?php elseif(is_page('analyze')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">analysis</span>
								</h2>
								<?php elseif(is_page('beginner')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">beginner</span>
								</h2>
								<?php elseif(is_page('flow')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">flow</span>
								</h2>
								<?php elseif(is_page('import')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">COSMETICS IMPORT AGENCY</span>
								</h2>
								<?php elseif(is_page('price')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">price</span>
								</h2>
								<?php elseif(is_page('check')) : ?>						
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small">check</span>
								</h2>
								<?php else :?>
								<h2 class="feature-title">
									<?php the_title(); ?><br /><span class="title-small"><?php the_slug();?></span>
								</h2>
								<?php endif; ?>			
							</div>
							<div class="page-breadcrumb clearfix">
							<?php if(function_exists('bcn_display'))
							{
								bcn_display();
							}?>
							</div>
						</div>
					</section>									
				
					 
				<?php endif; ?>			   
					<section id="content"><!-- begin content -->
					<div class="wrapper two-cols-left clearfix"><!-- begin two-cols -->
						<main class="primary"><!-- begin primary -->
		   