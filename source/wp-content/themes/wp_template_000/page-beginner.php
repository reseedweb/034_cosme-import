<?php get_header(); ?>
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<p>化粧品輸入代行.comは「海外で見つけた魅力的な化粧品を日本で販売したい」という他業種の輸入業者様・国内の小売/卸売業者様・個人事業主様や「新製品を日本でも販 売したい」という海外メーカー様が、より早くよりローコストで日本で化粧品を販売できるよう薬事法適法業務(薬事法チェック)を中心に化粧品輸入代行・化粧品 成分分析のサポートサービスを行っております。</p>
		<p>輸入の品目数・数量に関わらず、ご依頼主様のご希望を丁寧にお伺いした上でお取引させていただきます。</p>
	</div><!-- end primary-row -->
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">化粧品輸入が初めての方へ</h3>
		<div class="message-right message-305 clearfix">
			<div class="image">
			    <img src="<?php bloginfo('template_url'); ?>/img/content/beginner_content_img1.jpg" alt="beginner" />
			</div>
			<div class="text">
				<p>医療機器や化粧品の輸入販売を行うには、薬剤師や医療機器経験者を常時雇用の義務の他、様々なコスト、管理的なリスクが大きい上、製品毎に手続きも繁雑で実際に行うのはかなり難しい状況です。</p>
				<p>その様な企業や個人に代わり、手続き等を行うのが化粧品輸入代行です。</p>
				<p>化粧品輸入代行を挟む事によって、化粧品の販売までの管理や手続き、時間的なコストが大幅に短縮出来て、販売者の負担もかなり軽減されます。</p>
				<p>更に人気のある海外の化粧品も簡単に店舗に並べられますので、お客様は店舗運営に集中していただくことが出来ます。</p>
			</div>
		</div><!-- end message-305 -->
		<div class="primary-row clearfix"><!-- begin primary-row -->
			<div class="beginner-info">			
				<div class="beginner-info-text">
					<ul>
						<li>輸入化粧品の販売に興味がある</li>
						<li>化粧品の輸入を行いたいがノウハウがなくサポートをお願いしたい</li>
						<li>テスト的に輸入して販売したいため、小ロットでお願いしたい</li>
						<li>英語ができないからあきらめている</li>
						<li>ネットショップで海外化粧品を販売したいが輸入許可取得は困難</li>				
					</ul>
				</div><!-- ./beginner-info-text -->
			</div><!-- end beginner-info -->
		</div><!-- end primary-row -->	
		<div class="beginner-textslyle1">
			化粧品輸入代行は複雑な化粧品輸入業務の<br />すべてをアウトソージングできます。
		</div>
	</div><!-- end primary-row -->	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h3 class="h3-title">個人輸入のリスク</h3>
		<p class="pb20">厚労省によると、個人での化粧品輸入は、下記のようなリスクがあります。</p>
		<ul class="beginner-list">
			<li>
				・<span class="beginner-clr">品質、有効性</span>及び安全性の確認の保証がない
			</li>
			<li>
				・品質等の確認が行われていない医薬品等は、期待する効果が<br /><span class="pl20">得られなかったり、<span class="beginner-clr">人体に有害な物質</span>が含まれている場合がある</span>
			</li>
			<li>
				・正規のメーカー品を偽った、偽造製品かもしれない
			</li>
			<li>
				・自己判断で使用して<span class="beginner-clr">副作用や不具合など</span>が起きると、適切な対処が<br /><span class="pl20">困難なおそれがある</span>
			</li>			
		</ul>
		<div class="beginner-arrow1">
			<img src="<?php bloginfo('template_url'); ?>/img/content/beginner_arrow1.png" alt="beginner" />
		</div>		
		<div class="beginner-textslyle2">
			こうした危険性を防ぐために適切な成分分析や<br />薬事法による厳正なチェックが必要です。
		</div>
		<div class="beginner-arrow2">
			<img src="<?php bloginfo('template_url'); ?>/img/content/beginner_arrow2.png" alt="beginner" />
		</div>
		<div class="beginner-textslyle3">
			世界16カ国以上から輸入代行の実績がある専門会社
		</div>
		<div class="beginner-textslyle4">
			化粧品輸入代行<span class="beginner-textslyle4-1">i</span><span class="beginner-textslyle4-2">nfo</span>にお任せください！
		</div>
	</div><!-- end primary-row -->	
	<?php get_template_part('part','guideproduct'); ?>	
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>