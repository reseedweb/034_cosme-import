<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-con"><!-- begin side-con -->
        <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_contact_bg.jpg" />
		<h2 class="side-con-title">INQUIRY</h2>
		<div class="side-con-text1">
			<p>ご質問、ご相談など<br />お気軽にお問い合わせください。</p>			
		</div>		
		<p class="side-con-text2">お急ぎの方は電話でも対応</p>
		<p class="side-con-text3">FAXからの依頼はこちらから</p>
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="sidebar" src="<?php bloginfo('template_url');?>/img/common/side_contact_btn.png" />
            </a>
        </div><!-- ./side-con-btn -->		
    </div><!-- end side-con -->
	<div class="side-con-text">
	 <p>株式会社 アインズラボ AINS-LABINC.<br />神戸市中央区栄町通五丁目1-17</p>
	</div>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<p>
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_service_img1.jpg" alt="sidebar" />
	</p>
	
	<p class="side-service">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_service_img2.jpg" alt="sidebar" />
	</p>
	
	<p class="side-service">
		<img src="<?php bloginfo('template_url'); ?>/img/common/side_service_img3.jpg" alt="sidebar" />
	</p>
</div><!-- end sidebar-row -->

<?php get_template_part('part','snavi'); ?>

<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
	<p>
		<a href="<?php bloginfo('url'); ?>/faq">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_content_faq.jpg" alt="sidebar" />
		</a>
	</p>
	
	<p class="side-service">
		<a href="<?php bloginfo('url'); ?>/blog">
			<img src="<?php bloginfo('template_url'); ?>/img/common/side_content_blog.jpg" alt="sidebar" />
		</a>
	</p>
</div><!-- end sidebar-row -->





