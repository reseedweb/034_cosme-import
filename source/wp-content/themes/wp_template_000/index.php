<?php get_header(); ?>
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info1">
		<p class="top-info1-text1">化粧品輸入代行の利用が初めてという方へ</p>
		<p class="top-info1-text2">初回プライスキャンペーン</p>
		<div class="top-info1-text3">
			<div class="top-info1-t3left">成分チェック（1品目）</div>
			<div class="top-info1-t3right">1,000<span class="top-info1-unit">円</span></div>
		</div>
		<div class="top-info1-text4">
			<div class="top-info1-t3left">成分チェック（1品目）</div>
			<div class="top-info1-t3right">1,000<span class="top-info1-unit">円</span></div>
		</div>
		<div class="top-info1-btn">
			<a href="<?php bloginfo('url'); ?>/price">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img1_btn.png" alt="top" />
			</a>
		</div>
	</div<!-- ./top-content-info1 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info2">
		<div class="top-info2-content">
			<div class="top-info2">
				<div class="top-info2-text1">ヒアリングの上、最適な<br />ご提案をさせていただきます！</div>
				<div class="top-info2-text2">まずはご相談の上、お客様が納得していただける最適な提案をさせていただきます。化粧品輸入代行・適法管理・薬事法チェックと豊富な実績を持つ当社だからそできるサービスを提供します。</div>
			</div>	
			<div class="top-info2 top-info2-space">
				<div class="top-info2-text1">海外に強いマネジメントで<br />化粧品輸入代行から納品まで</div>
				<div class="top-info2-text2">世界16か国以上から化粧品の輸入代行・適法管理をさせていただいており、薬事法関連業務はもちろん、貿易手配や外国製造業者との連絡代行も豊富な実績を基にリスク管理のアドバイスも可能です。</div>
			</div>	
			<div class="top-info2 top-info2-space">
				<div class="top-info2-text1">スムーズな対応力</div>
				<div class="top-info2-text2">当社では、分析機関や印刷会社と常に連絡を取り、また、納期に応じて自社でのラベル印刷も行っております。そのため、品質管理や資材調達もスピーディーに実施することができます。</div>
			</div>	
		</div>		
	</div><!-- ./top-content-info2 -->
</div><!-- end primary-row -->

<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="top-content-info3"><!-- begin top-content-info3 -->
		<div class="top-info3-title">初めての方へのご案内</div>
		<div class="top-info3-text">
			<ul>
				<li>輸入化粧品の販売に興味がある</li>
				<li>化粧品の輸入を行いたいがノウハウがなくサポートをお願いしたい</li>
				<li>テスト的に輸入して販売したいため、小ロットでお願いしたい</li>
				<li>英語ができないからあきらめている</li>
				<li>ネットショップで海外化粧品を販売したいが輸入許可取得は困難</li>				
			</ul>
		</div><!-- ./top-info-text -->
	</div><!-- end top-content-info3 -->
	
	<div class="top-content-info4"><!-- begin top-content-info4 -->
		<div class="top-info4-title">初めての方でも安心</div>
		<div class="top-info4-text1">
			化粧品輸入代行は全てお任せ下さい。
		</div><!-- ./top-info-text -->
		<div class="top-info4-text2">
			世界16カ国以上から輸入代行の<br />実績がある専門会社。
		</div>
	</div><!-- end top-content-info4 -->
	
	<div class="top-content-info5"><!-- begin top-content-info5 -->
		<div class="top-info5-btn">
			<a href="<?php bloginfo('url'); ?>/contact">
				<img src="<?php bloginfo('template_url'); ?>/img/content/top_content_img5_btn.png" alt="top" />
			</a>
		</div>
	</div><!-- ./top-content-info5 -->
</div><!-- end primary-row -->

<?php get_template_part('part','guideproduct'); ?>

<?php get_template_part('part','contact'); ?>

<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="mb20"><img src="<?php bloginfo('template_url'); ?>/img/content/top_bottom_tittle.jpg" alt="top" /></h2>
	<div class="top-bottom-text"><!-- begin top-content-info5 -->
		<p>化粧品輸入代行infoは「海外で見つけた魅力的な化粧品を日本で販売したい」という他業種の輸入業者様・国内の小売/卸売業者様・個人事業主様や「新製品を日本でも販 売したい」という海外メーカー様が、より早くよりローコストで日本で化粧品を販売できるよう薬事法適法業務を中心に化粧品輸入に関わるさまざまなサポートサービスを行っております。輸入の品目数・数量に関わらず、ご依頼主様のご希望を丁寧にお伺いした上でお取引させていただきます。</p>
		<p class="pt20">メーカー様・販売業者様とのやりとりや法律に基づく諸手続き、義務づけられている商品への表示やパッケージなど化粧品輸入に関するあらゆるサービスを行っておりますので、お気軽にお問い合わせください。</p>
	</div<!-- ./top-content-info5 -->
</div><!-- end primary-row -->

<?php get_footer(); ?>