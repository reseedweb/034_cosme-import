
                    </main><!-- end primary -->
                    <aside class="sidebar"><!-- begin sidebar -->
                        <?php if(is_page('blog') || is_category() || is_single()) : ?>
                            <?php
                            $queried_object = get_queried_object();                                
                            $sidebar_part = 'blog';
                            if(is_tax() || is_archive()){                                    
                                $sidebar_part = '';
                            }                               

                            if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                $sidebar_part = '';
                            }   
                                    
                            if($queried_object->taxonomy == 'category'){                                    
                                $sidebar_part = 'blog';
                            }                 
                            ?>
                            <?php get_template_part('sidebar',$sidebar_part); ?>  
                        <?php else: ?>
                            <?php get_template_part('sidebar'); ?>  
                        <?php endif; ?>                                    
                    </aside>                            
                </div><!-- end wrapper -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <div class="wrapper footer-content clearfix"><!-- begin wrapper -->                    
                    <div class="footer-content1 clearfix">
						<div class="footer-content1-text">化粧品輸入代行・化粧品成分分析・薬事法チェックならお任せ下さい。</div>
						<div class="footer-content1-btn">
							<a href="<?php bloginfo('url'); ?>/contact">
								<img src="<?php bloginfo('template_url'); ?>/img/common/footer_con.jpg" alt="top" />
							</a>
						</div>
					</div><!-- ./footer-content1 -->
					
					<div class="footer-content2 clearfix">
						<div class="footer-content2-logo">
							<a href="<?php bloginfo('url'); ?>/">
								<img src="<?php bloginfo('template_url'); ?>/img/common/footer_logo.jpg" alt="top" />
							</a>
						</div>
						<div class="footer-content2-tel">
							<img src="<?php bloginfo('template_url'); ?>/img/common/footer_tel.jpg" alt="top" />
						</div>
					</div><!-- ./footer-content2 -->				
                </div><!-- end wrapper -->		
				<div class="footer-content-navi1 clearfix">
					<div class="wrapper text-center clearfix">
						<ul>
							<li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
							<li><a href="<?php bloginfo('url'); ?>/beginner">初めての方へ</a></li>
							<li><a href="<?php bloginfo('url'); ?>/import">化粧品輸入代行</a></li>  
							<li><a href="<?php bloginfo('url'); ?>/analyze">成分分析</a></li>  
							<li><a href="<?php bloginfo('url'); ?>/check">薬事法チェック</a></li> 
							<li><a href="<?php bloginfo('url'); ?>/price">初回プライスキャンペーン</a></li>  							
							<li><a href="<?php bloginfo('url'); ?>/flow">ご依頼の流れ</a></li>  
							<li><a href="<?php bloginfo('url'); ?>/contact">問い合わせ</a></li>  
							<li><a href="<?php bloginfo('url'); ?>/faq">よくある質問</a></li>  
							<li><a href="<?php bloginfo('url'); ?>/blog">ブログ</a></li>
						</ul>						  
					</div >
				</div><!-- ./footer-content-navi1 -->
				<div class="footer-content-navi2 clearfix">
					<div class="wrapper clearfix">
						<ul>
							<li><a href="<?php bloginfo('url'); ?>/company">運営会社</a></li>
							<li><a href="<?php bloginfo('url'); ?>/privacy">プライバシーポリシー</a></li>							
						</ul>
					</div>					
				</div><!-- ./footer-content-navi2 -->
                <div class="footer-copyright clearfix"><!-- begin wrapper -->
                    Copyright © 2015 株式会社 アインズラボ AINS-LAB INC.  All Rights Reserved.
                </div>         
            </footer><!-- end footer -->            			
        </div><!-- end wrapper -->
        <?php wp_footer(); ?>         		
    </body>
</html>